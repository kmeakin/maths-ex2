all:
	pandoc src/*.md \
	-o out/report.pdf \
	--highlight-style pygments \
	-V fontsize=12pt \
	-V papersize=a4paper \
	--pdf-engine=xelatex \

