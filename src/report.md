---
geometry: margin=1cm
monofont: 'Fira Mono'
author: "Karl Meakin"
---

# Report
The equation to be solved:
$$
\frac{\partial^{2} \phi(x, t)}{\partial x^{2}} = \frac{1}{k} \frac{\partial \phi(x,
t)}{\partial t}
$$

## Part 1
### 1.1
Central difference equation of the second-order spatial derivative:
$$ 
\frac{\partial^{2} \phi(x, t)}{\partial x^{2}}\bigg{|}_{i, j} \approx \frac{\phi(i-1, j) +
\phi(i + 1, j)}{(\Delta x)^{2}} 
$$

Forward difference approximation of the first-order time deriviative:
$$
\frac{\partial \phi(x, t)}{\partial t}\bigg{|}_{i, j} \approx \frac{\phi(i, j+1) - \phi(i, j)}{\Delta t}
$$

Update equation:
$$
\phi(i, j+1) = (1-2 \gamma) \phi(i, j) + \gamma (\phi(i-1, j) + \phi(i + 1, j))
$$

where $\gamma = \frac{k \Delta t}{(\Delta x)^{2}}$

### 1.2
Computational molecule:

![](img/1.2-computational-molecule.png)

### 1.3
Explicit finite difference algorithm:

* Create a grid with $\frac{total\_time}{\Delta t}$ rows and
  $\frac{total\_length}{\Delta x}$ columns.
* Initialize the interior nodes to 500.0, and the boundary nodes at $x=0$ and
  $x=total\_length$ to 0.0.
* For each interior row (ie for $j$ from $1$ to $n\_rows$):
    * For each interior node (ie for $i$ from $1$ to $n\_columns-1$):
        * $\phi(i, j) = (1-2 \gamma) \phi(i, j-1) + \gamma (\phi(i-1, j-1) +
          \phi(i + 1, j-1))$
        
### 1.4 and 1.5
Temperature distribution:

![](out/part1-temperature-distribution.png)

Temperature evolution:

![](out/part1-temperature-evolution_a_and_b.png)
![](out/part1-temperature-evolution_c.png)

Temperature at $x=20cm, t=600s$:

|$\Delta t, \Delta x$               | Temperature
|-----------------------------------|------------
|$\Delta t = 100s, \Delta x = 20cm$ | $220.96206625330822$
|$\Delta t = 50, \Delta x = 20cm$   | $225.04696307688005$
|$\Delta t = 100s, \Delta x = 10cm$ | $-1995.6567877944126$

The temperature evolution for $\Delta t = 100s, \Delta x = 10cm$ had to be plotted
on a separate graph, because it is unstable. Plotting in on the same graph as
$\Delta t = 100s, \Delta x = 20cm$ and $\Delta t = 50, \Delta x = 20cm$ would
result in the first two results being too small to see.

TODO: calculate stability conditions

## Part 2

### 2.1
Finite difference equations:

$$
\frac{\partial \phi(x, t)}{\partial t}\bigg{|}_{i, j + \frac{1}{2}} \approx
\frac{\phi(i, j + 1) - \phi(i, j)}{\Delta t}
$$

$$
\frac{\partial^{2} \phi(x, t)}{\partial x^{2}}\bigg{|}_{i, j + \frac{1}{2}} \approx
\frac{\phi(i-1, j) - 2\phi(i, j) + \phi(i + 1, j) + \phi(i-1 ,j+1) - 2\phi(i ,j+1) + \phi(i+1,
j+1)}{2 (\Delta x)^{2}}
$$

Update equation:
$$
\phi(i,j+1) = \phi(i,j) + \gamma (\phi(i-1,j) - 2 \phi(i,j) + \phi(i+1,j) +
\phi(i-1,j+1) - 2\phi(i,j+1) + \phi(i+1,j+1))
$$

where $\gamma = \frac{k \Delta t}{2 (\Delta x)^{2}}$

### 2.2
Computational molecule:

![](img/2.2-computational-molecule.png)

### 2.3
Crank-Nicholson algorithm:
TODO

### 2.4 and 2.5
Temperature distribution:

![](out/part2-temperature-distribution.png)

Temperature evolution:

![](out/part2-temperature-evolution.png)

## Part 3
TODO