#!/usr/bin/env python3

import numpy as np
import sys

np.set_printoptions(threshold=sys.maxsize)

K = 0.835


def implicit(total_length, total_time, delta_x, delta_t):
    length = int(total_length / delta_x + 1)
    time = int(total_time / delta_t + 1)
    grid = np.zeros((time, length))
    grid[0, 1 : length - 1] = 500
    grid[1:time, 1 : length - 1] = 1
    gamma = delta_t * K / (delta_x * delta_x * 2)

    B = np.zeros((time - 1, length - 2))
    A = []

    for t in range(0, time - 1):
        for x in range(1, length - 1):
            a = np.zeros((time - 1, length - 2))
            rhs = 0

            def f(delta_x, delta_t, gamma):
                nonlocal rhs

                if grid[t + delta_t, x + delta_x] == 1:
                    a[t + delta_t - 1, x + delta_x - 1] = gamma
                else:
                    rhs -= grid[t + delta_t, x + delta_x] * gamma

            f(-1, +1, gamma)
            f(0, +1, -2 * gamma - 1)
            f(+1, +1, gamma)
            f(-1, 0, gamma)
            f(0, 0, -2 * gamma + 1)
            f(+1, 0, gamma)

            A.append(a.reshape(-1))

            B[t, x - 1] = rhs

    A = np.array(A)
    B = B.reshape(-1)

    x = np.linalg.solve(A, B)
    x = np.reshape(x, (time - 1, length - 2))
    return x


print(implicit(100, 60, 20, 10))

print(implicit(100, 600, 20, 100))
print(implicit(100, 600, 20, 50))
print(implicit(100, 600, 10, 100))
