#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    missing_copy_implementations,
    missing_debug_implementations,
    rust_2018_idioms,
    unused_qualifications
)]
#![allow(
    clippy::cast_possible_truncation,
    clippy::cast_precision_loss,
    clippy::cast_sign_loss,
    clippy::must_use_candidate,
    clippy::non_ascii_literal,
    clippy::suboptimal_flops,
    clippy::suspicious_operation_groupings,
    clippy::wildcard_imports,
    elided_lifetimes_in_paths
)]

use assignment2::*;
use ndarray::arr2;

fn main() {
    let total_length = 100.0;
    let total_time = 60.0;
    let delta_x = 20.0;
    let delta_t = 10.0;
    let explicit = explicit(total_length, total_time, delta_x, delta_t);

    let implicit = arr2(&[
        [0.0, 500.0, 500.0, 500.0, 500.0, 0.0],
        [
            0.0,
            412.93997414,
            491.77191606,
            491.77191606,
            412.93997414,
            0.0,
        ],
        [
            0.0,
            353.46779462,
            471.25028472,
            471.25028472,
            353.46779462,
            0.0,
        ],
        [
            0.0,
            310.49200877,
            444.92526608,
            444.92526608,
            310.49200877,
            0.0,
        ],
        [
            0.0,
            277.62439361,
            416.40823175,
            416.40823175,
            277.62439361,
            0.0,
        ],
        [
            0.0,
            251.16561877,
            387.67454904,
            387.67454904,
            251.16561877,
            0.0,
        ],
        [
            0.0,
            228.95517637,
            359.77238346,
            359.77238346,
            228.95517637,
            0.0,
        ],
    ]);

    dbg!(&explicit);
    dbg!(&implicit);

    plot_temperature_distribution(
        "out/part3-temperature-distribution.png",
        &explicit,
        total_length,
        total_time,
        delta_x,
        delta_t,
    )
    .unwrap();

    plot_temperature_evolution(
        "out/part3-temperature-evolution.png",
        total_length,
        total_time,
        &[(&explicit, delta_x, delta_t), (&implicit, delta_x, delta_t)],
    )
    .unwrap();
}
