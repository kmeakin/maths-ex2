#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    missing_copy_implementations,
    missing_debug_implementations,
    rust_2018_idioms,
    unused_qualifications
)]
#![allow(
    clippy::cast_possible_truncation,
    clippy::cast_precision_loss,
    clippy::cast_sign_loss,
    clippy::must_use_candidate,
    clippy::non_ascii_literal,
    clippy::suboptimal_flops,
    clippy::suspicious_operation_groupings,
    clippy::wildcard_imports,
    elided_lifetimes_in_paths
)]

use assignment2::*;

fn main() {
    let total_length = 100.0;
    let total_time = 600.0;

    let explicit_a = explicit(total_length, total_time, 20.0, 100.0);
    let explicit_b = explicit(total_length, total_time, 20.0, 50.0);
    let explicit_c = explicit(total_length, total_time, 10.0, 100.0);

    plot_temperature_distribution(
        "out/part1-temperature-distribution.png",
        &explicit_a,
        total_length,
        total_time,
        20.0,
        100.0,
    )
    .unwrap();

    plot_temperature_evolution(
        "out/part1-temperature-evolution_a_and_b.png",
        total_length,
        total_time,
        &[(&explicit_a, 20.0, 100.0), (&explicit_b, 20.0, 50.0)],
    )
    .unwrap();

    plot_temperature_evolution(
        "out/part1-temperature-evolution_c.png",
        total_length,
        total_time,
        &[(&explicit_c, 10.0, 100.0)],
    )
    .unwrap();

    dbg!(explicit_a);
    dbg!(explicit_b);
    dbg!(explicit_c);
}
