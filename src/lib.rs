#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    missing_copy_implementations,
    missing_debug_implementations,
    rust_2018_idioms,
    unused_qualifications
)]
#![allow(
    clippy::cast_possible_truncation,
    clippy::cast_precision_loss,
    clippy::cast_sign_loss,
    clippy::must_use_candidate,
    clippy::non_ascii_literal,
    clippy::suboptimal_flops,
    clippy::suspicious_operation_groupings,
    clippy::wildcard_imports,
    elided_lifetimes_in_paths
)]
#![feature(total_cmp)]

use ndarray::{Array, Array2};
use plotters::{coord::types::RangedCoordf64, prelude::*};
use plotters_bitmap::BitMapBackend;
use std::error::Error;

pub const K: f64 = 0.835;
pub const INITIAL_TEMP: f64 = 500.0;
pub type Grid = Array2<f64>;

pub const COLORS: &[RGBColor] = &[RED, GREEN, BLUE, YELLOW, CYAN, MAGENTA];

pub fn analytic(total_length: f64, total_time: f64, x: f64, t: f64) -> f64 {
    let pi = std::f64::consts::PI;
    let sum: f64 = (0..1_000)
        .map(|n| {
            let n = n * 2 + 1;
            let n = f64::from(n);
            let sin = n * pi * x / total_length;
            let exp = -(n * n * pi * pi * K) / (total_length * total_length) * t;
            1.0 / n * sin.sin() * exp.exp()
        })
        .sum();
    2000.0 * sum / pi
}

pub fn dim(total_length: f64, total_time: f64, delta_x: f64, delta_t: f64) -> (usize, usize) {
    (
        (total_length / delta_x + 1.0) as _,
        (total_time / delta_t + 1.0) as _,
    )
}

pub fn new_grid(total_length: f64, total_time: f64, delta_x: f64, delta_t: f64) -> Grid {
    let (length, time) = dim(total_length, total_time, delta_x, delta_t);
    let mut grid = Array::from_elem((time, length), INITIAL_TEMP);

    for t in 0..time {
        grid[(t, 0)] = 0.0;
        grid[(t, length - 1)] = 0.0;
    }

    grid
}

pub fn explicit(total_length: f64, total_time: f64, delta_x: f64, delta_t: f64) -> Grid {
    let mut grid = new_grid(total_length, total_time, delta_x, delta_t);
    let (length, time) = dim(total_length, total_time, delta_x, delta_t);

    let gamma = (K * delta_t) / (delta_x * delta_x);
    for t in 1..time {
        for x in 1..length - 1 {
            let new_temp = (1.0 - 2.0 * gamma) * grid[(t - 1, x)]
                + gamma * (grid[(t - 1, x - 1)] + grid[(t - 1, x + 1)]);
            grid[(t, x)] = new_temp;
        }
    }
    grid
}

pub fn plot_temperature_distribution(
    name: &str,
    grid: &Grid,
    total_length: f64,
    total_time: f64,
    delta_x: f64,
    delta_t: f64,
) -> Result<(), Box<dyn Error>> {
    let (length, time) = dim(total_length, total_time, delta_x, delta_t);

    let root = plotters_bitmap::BitMapBackend::new(name, (1920, 1080)).into_drawing_area();
    root.fill(&WHITE)?;

    let mut chart = ChartBuilder::on(&root)
        .caption("Temperature distribution", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0.0..total_length, 0.0..INITIAL_TEMP)?;

    chart
        .configure_mesh()
        .x_desc("Distance (cm)")
        .y_desc("Temperature (C)")
        .draw()?;

    for t in 0..time {
        let color = COLORS.iter().cycle().nth(t).unwrap();
        chart
            .draw_series(LineSeries::new(
                (0..length).map(|x| {
                    let temp = grid[(t, x)];
                    (x as f64 * delta_x, temp)
                }),
                color,
            ))?
            .label(format!("t = {}s", t as f64 * delta_t))
            .legend(move |(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], color));
    }

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}

pub fn plot_temperature_evolution(
    name: &str,
    total_length: f64,
    total_time: f64,
    grids: &[(&Grid, f64, f64)],
) -> Result<(), Box<dyn Error>> {
    fn draw<'a>(
        chart: &mut ChartContext<'a, BitMapBackend, Cartesian2d<RangedCoordf64, RangedCoordf64>>,
        grid: &Grid,
        total_length: f64,
        total_time: f64,
        delta_x: f64,
        delta_t: f64,
        color: &'a RGBColor,
    ) -> Result<(), Box<dyn Error>> {
        let (_length, time) = dim(total_length, total_time, delta_x, delta_t);

        chart
            .draw_series(LineSeries::new(
                (0..time).map(|t| {
                    let x = 1;
                    let temp = grid[(t, x)];
                    (t as f64 * delta_t, temp)
                }),
                color,
            ))?
            .label(format!("Δt = {}s, Δx = {}cm", delta_t, delta_x))
            .legend(move |(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], color));
        Ok(())
    }

    let root = plotters_bitmap::BitMapBackend::new(name, (1920, 1080)).into_drawing_area();
    root.fill(&WHITE)?;

    let max_temp = grids
        .iter()
        .map(|(grid, ..)| grid)
        .filter_map(|grid| grid.iter().max_by(|x, y| x.total_cmp(y)))
        .max_by(|x, y| x.total_cmp(y))
        .copied()
        .unwrap();

    let min_temp = grids
        .iter()
        .map(|(grid, ..)| grid)
        .filter_map(|grid| grid.iter().min_by(|x, y| x.total_cmp(y)))
        .min_by(|x, y| x.total_cmp(y))
        .copied()
        .unwrap();

    let mut chart = ChartBuilder::on(&root)
        .caption("Temperature evolution", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0.0..total_time, min_temp..max_temp)?;

    chart
        .configure_mesh()
        .x_desc("Time (s)")
        .y_desc("Temperature (C)")
        .draw()?;

    chart
        .draw_series(LineSeries::new(
            (0..total_time as u32).map(|time| {
                let time = time.into();
                let x = 20.0;
                let temp = analytic(total_length, total_time, x, time);
                (time, temp)
            }),
            &BLACK,
        ))?
        .label("Analytic")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &BLACK));

    for (i, (grid, delta_x, delta_t)) in grids.iter().enumerate() {
        draw(
            &mut chart,
            grid,
            total_length,
            total_time,
            *delta_x,
            *delta_t,
            COLORS.iter().cycle().nth(i).unwrap(),
        )?;
    }

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}
